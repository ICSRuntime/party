package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.PartyService;
	
import com.digitalml.rest.resources.codegentest.service.PartyService.GetASpecificPartyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.GetASpecificPartyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.GetASpecificPartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.FindPartyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.FindPartyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.FindPartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.UpdatePartyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.UpdatePartyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.UpdatePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.ReplacePartyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.ReplacePartyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.ReplacePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.CreatePartyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.CreatePartyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.CreatePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.DeletePartyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.DeletePartyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.DeletePartyInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Party
	 * Access and update of customer or party information.
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	
	@Path("http://digitalml.com/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class PartyResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(PartyResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private PartyService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Party.PartyServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private PartyService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof PartyService)) {
			LOGGER.error(implementationClass + " is not an instance of " + PartyService.class.getName());
			return null;
		}

		return (PartyService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificparty
		Gets party details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/party/{id}")
	public javax.ws.rs.core.Response getaspecificparty(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificPartyInputParametersDTO inputs = new PartyService.GetASpecificPartyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificPartyReturnDTO returnValue = delegateService.getaspecificparty(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findparty
		Gets a collection of party details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/party")
	public javax.ws.rs.core.Response findparty(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindPartyInputParametersDTO inputs = new PartyService.FindPartyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindPartyReturnDTO returnValue = delegateService.findparty(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateparty
		Updates party

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/party/{id}")
	public javax.ws.rs.core.Response updateparty(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdatePartyInputParametersDTO inputs = new PartyService.UpdatePartyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdatePartyReturnDTO returnValue = delegateService.updateparty(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceparty
		Replaces party

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/party/{id}")
	public javax.ws.rs.core.Response replaceparty(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplacePartyInputParametersDTO inputs = new PartyService.ReplacePartyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplacePartyReturnDTO returnValue = delegateService.replaceparty(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createparty
		Creates party

	Non-functional requirements:
	*/
	
	@POST
	@Path("/party")
	public javax.ws.rs.core.Response createparty(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreatePartyInputParametersDTO inputs = new PartyService.CreatePartyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreatePartyReturnDTO returnValue = delegateService.createparty(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteparty
		Deletes party

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/party/{id}")
	public javax.ws.rs.core.Response deleteparty(
		@PathParam("id")@NotEmpty String id) {

		DeletePartyInputParametersDTO inputs = new PartyService.DeletePartyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeletePartyReturnDTO returnValue = delegateService.deleteparty(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}