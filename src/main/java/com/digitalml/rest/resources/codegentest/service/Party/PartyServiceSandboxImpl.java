package com.digitalml.rest.resources.codegentest.service.Party;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.PartyService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Party
 * Access and update of customer or party information.
 *
 * @author admin
 * @version 1.0
 *
 */

public class PartyServiceSandboxImpl extends PartyService {
	

    public GetASpecificPartyCurrentStateDTO getaspecificpartyUseCaseStep1(GetASpecificPartyCurrentStateDTO currentState) {
    

        GetASpecificPartyReturnStatusDTO returnStatus = new GetASpecificPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificPartyCurrentStateDTO getaspecificpartyUseCaseStep2(GetASpecificPartyCurrentStateDTO currentState) {
    

        GetASpecificPartyReturnStatusDTO returnStatus = new GetASpecificPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificPartyCurrentStateDTO getaspecificpartyUseCaseStep3(GetASpecificPartyCurrentStateDTO currentState) {
    

        GetASpecificPartyReturnStatusDTO returnStatus = new GetASpecificPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificPartyCurrentStateDTO getaspecificpartyUseCaseStep4(GetASpecificPartyCurrentStateDTO currentState) {
    

        GetASpecificPartyReturnStatusDTO returnStatus = new GetASpecificPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificPartyCurrentStateDTO getaspecificpartyUseCaseStep5(GetASpecificPartyCurrentStateDTO currentState) {
    

        GetASpecificPartyReturnStatusDTO returnStatus = new GetASpecificPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificPartyCurrentStateDTO getaspecificpartyUseCaseStep6(GetASpecificPartyCurrentStateDTO currentState) {
    

        GetASpecificPartyReturnStatusDTO returnStatus = new GetASpecificPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindPartyCurrentStateDTO findpartyUseCaseStep1(FindPartyCurrentStateDTO currentState) {
    

        FindPartyReturnStatusDTO returnStatus = new FindPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindPartyCurrentStateDTO findpartyUseCaseStep2(FindPartyCurrentStateDTO currentState) {
    

        FindPartyReturnStatusDTO returnStatus = new FindPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindPartyCurrentStateDTO findpartyUseCaseStep3(FindPartyCurrentStateDTO currentState) {
    

        FindPartyReturnStatusDTO returnStatus = new FindPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindPartyCurrentStateDTO findpartyUseCaseStep4(FindPartyCurrentStateDTO currentState) {
    

        FindPartyReturnStatusDTO returnStatus = new FindPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindPartyCurrentStateDTO findpartyUseCaseStep5(FindPartyCurrentStateDTO currentState) {
    

        FindPartyReturnStatusDTO returnStatus = new FindPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindPartyCurrentStateDTO findpartyUseCaseStep6(FindPartyCurrentStateDTO currentState) {
    

        FindPartyReturnStatusDTO returnStatus = new FindPartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep1(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep2(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep3(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep4(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep5(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep6(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdatePartyCurrentStateDTO updatepartyUseCaseStep7(UpdatePartyCurrentStateDTO currentState) {
    

        UpdatePartyReturnStatusDTO returnStatus = new UpdatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep1(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep2(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep3(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep4(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep5(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep6(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplacePartyCurrentStateDTO replacepartyUseCaseStep7(ReplacePartyCurrentStateDTO currentState) {
    

        ReplacePartyReturnStatusDTO returnStatus = new ReplacePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreatePartyCurrentStateDTO createpartyUseCaseStep1(CreatePartyCurrentStateDTO currentState) {
    

        CreatePartyReturnStatusDTO returnStatus = new CreatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePartyCurrentStateDTO createpartyUseCaseStep2(CreatePartyCurrentStateDTO currentState) {
    

        CreatePartyReturnStatusDTO returnStatus = new CreatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePartyCurrentStateDTO createpartyUseCaseStep3(CreatePartyCurrentStateDTO currentState) {
    

        CreatePartyReturnStatusDTO returnStatus = new CreatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePartyCurrentStateDTO createpartyUseCaseStep4(CreatePartyCurrentStateDTO currentState) {
    

        CreatePartyReturnStatusDTO returnStatus = new CreatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePartyCurrentStateDTO createpartyUseCaseStep5(CreatePartyCurrentStateDTO currentState) {
    

        CreatePartyReturnStatusDTO returnStatus = new CreatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreatePartyCurrentStateDTO createpartyUseCaseStep6(CreatePartyCurrentStateDTO currentState) {
    

        CreatePartyReturnStatusDTO returnStatus = new CreatePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public DeletePartyCurrentStateDTO deletepartyUseCaseStep1(DeletePartyCurrentStateDTO currentState) {
    

        DeletePartyReturnStatusDTO returnStatus = new DeletePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeletePartyCurrentStateDTO deletepartyUseCaseStep2(DeletePartyCurrentStateDTO currentState) {
    

        DeletePartyReturnStatusDTO returnStatus = new DeletePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeletePartyCurrentStateDTO deletepartyUseCaseStep3(DeletePartyCurrentStateDTO currentState) {
    

        DeletePartyReturnStatusDTO returnStatus = new DeletePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeletePartyCurrentStateDTO deletepartyUseCaseStep4(DeletePartyCurrentStateDTO currentState) {
    

        DeletePartyReturnStatusDTO returnStatus = new DeletePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeletePartyCurrentStateDTO deletepartyUseCaseStep5(DeletePartyCurrentStateDTO currentState) {
    

        DeletePartyReturnStatusDTO returnStatus = new DeletePartyReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of customer or party information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = PartyService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}