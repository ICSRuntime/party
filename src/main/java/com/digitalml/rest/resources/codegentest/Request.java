package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Request:
{
  "required": [
    "party"
  ],
  "type": "object",
  "properties": {
    "party": {
      "$ref": "Party"
    }
  }
}
*/

public class Request {

	@Size(max=1)
	@NotNull
	private com.digitalml.insurance.common.party.Party party;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    party = new com.digitalml.insurance.common.party.Party();
	}
	public com.digitalml.insurance.common.party.Party getParty() {
		return party;
	}
	
	public void setParty(com.digitalml.insurance.common.party.Party party) {
		this.party = party;
	}
}