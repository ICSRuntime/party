package com.digitalml.insurance.common.party;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Party:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "fullName": {
      "type": "string"
    },
    "governmentID": {
      "type": "string"
    },
    "governmentIDType": {
      "type": "string"
    }
  }
}
*/

public class Party {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String fullName;

	@Size(max=1)
	private String governmentID;

	@Size(max=1)
	private String governmentIDType;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    fullName = null;
	    governmentID = null;
	    governmentIDType = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGovernmentID() {
		return governmentID;
	}
	
	public void setGovernmentID(String governmentID) {
		this.governmentID = governmentID;
	}
	public String getGovernmentIDType() {
		return governmentIDType;
	}
	
	public void setGovernmentIDType(String governmentIDType) {
		this.governmentIDType = governmentIDType;
	}
}