package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Party.PartyServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.PartyService.DeletePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.DeletePartyReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeletePartyTests {

	@Test
	public void testOperationDeletePartyBasicMapping()  {
		PartyServiceDefaultImpl serviceDefaultImpl = new PartyServiceDefaultImpl();
		DeletePartyInputParametersDTO inputs = new DeletePartyInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeletePartyReturnDTO returnValue = serviceDefaultImpl.deleteparty(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}