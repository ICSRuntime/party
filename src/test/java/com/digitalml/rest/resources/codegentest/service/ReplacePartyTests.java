package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Party.PartyServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.PartyService.ReplacePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.ReplacePartyReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class ReplacePartyTests {

	@Test
	public void testOperationReplacePartyBasicMapping()  {
		PartyServiceDefaultImpl serviceDefaultImpl = new PartyServiceDefaultImpl();
		ReplacePartyInputParametersDTO inputs = new ReplacePartyInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		ReplacePartyReturnDTO returnValue = serviceDefaultImpl.replaceparty(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}