package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Party.PartyServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.PartyService.GetASpecificPartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.GetASpecificPartyReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificPartyTests {

	@Test
	public void testOperationGetASpecificPartyBasicMapping()  {
		PartyServiceDefaultImpl serviceDefaultImpl = new PartyServiceDefaultImpl();
		GetASpecificPartyInputParametersDTO inputs = new GetASpecificPartyInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificPartyReturnDTO returnValue = serviceDefaultImpl.getaspecificparty(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}