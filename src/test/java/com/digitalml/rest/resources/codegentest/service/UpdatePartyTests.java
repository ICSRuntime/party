package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Party.PartyServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.PartyService.UpdatePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.UpdatePartyReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdatePartyTests {

	@Test
	public void testOperationUpdatePartyBasicMapping()  {
		PartyServiceDefaultImpl serviceDefaultImpl = new PartyServiceDefaultImpl();
		UpdatePartyInputParametersDTO inputs = new UpdatePartyInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		UpdatePartyReturnDTO returnValue = serviceDefaultImpl.updateparty(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}