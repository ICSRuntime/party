package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Party.PartyServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.PartyService.CreatePartyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.PartyService.CreatePartyReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreatePartyTests {

	@Test
	public void testOperationCreatePartyBasicMapping()  {
		PartyServiceDefaultImpl serviceDefaultImpl = new PartyServiceDefaultImpl();
		CreatePartyInputParametersDTO inputs = new CreatePartyInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreatePartyReturnDTO returnValue = serviceDefaultImpl.createparty(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}